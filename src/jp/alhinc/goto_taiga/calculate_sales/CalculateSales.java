package jp.alhinc.goto_taiga.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		List<String> uriagesyukeiList = new ArrayList<String>();
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		File file = new File(args[0], "branch.lst");
		BufferedReader filereader1 = null;
		BufferedReader filereader2 = null;
		BufferedWriter filereader3 = null;

		// 支店定義ファイルの読み込み

		int status = 0;
		try {

			if (file.exists()) {

				String line;
				String[] filedata = null;
				filereader1 = new BufferedReader(new FileReader(file));

				while ((line = filereader1.readLine()) != null) {

					filedata = line.split(",");

					if (filedata[0].matches("[0-9]{3}") && filedata.length == 2) {

						map1.put(filedata[0], filedata[1]);

					} else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}

			} else {
				System.out.println("支店定義ファイルが存在しません");
				status = 1;
				return;

			}

		} catch (IOException e) {

			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				if (status == 1) {
					return;
				}
				filereader1.close();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		//売り上げファイルの読み込み

		FilenameFilter filter = new FilenameFilter() {

			public boolean accept(File file, String str) {

				if (str.matches("[0-9]{8}.rcd")) {
					return true;

				} else {
					return false;
				}
			}
		};

		File[] list = new File(args[0]).listFiles(filter);

		for (int i = 0; i <= list.length - 2; i++) {

			String filename1 = list[i].getName();
			String filename2 = list[i + 1].getName();

			String[] name1 = filename1.split("\\.");
			String[] name2 = filename2.split("\\.");

			int filenum = Integer.parseInt(name1[0]);
			int filenextnum = Integer.parseInt(name2[0]);
			int sabun = filenextnum - filenum;

			if (sabun != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}

		long sum = 0;
		String line;
		String line2;
		String line3;
		try {

			for (int i = 0; i < list.length; i++) {
				filereader2 = new BufferedReader(new FileReader(list[i]));

				line = filereader2.readLine();
				line2 = filereader2.readLine();
				line3 = filereader2.readLine();
				String goukei = null;

				if (!(line3 == null)) {
					System.out.println(list[i] + "フォーマットが不正です");
					return;
				}

				//支店コード処理
				if (map1.containsKey(line)) {
					map2.put(line, line2);
				} else {
					System.out.println(list[i] + "の支店コードが不正です");
					return;
				}

				if (map2.containsKey(line)) {
					long motone = Long.parseLong(map2.get(line));
					long atone = Long.parseLong(line2);

					sum = motone + atone;
					goukei = String.valueOf(sum);
				}
				if (goukei.length() > 9) {
					System.out.println("合計金額が10桁を超えました");
					return;
				} else {

					map2.put(line, goukei);
				}
			}

		} catch (IOException e) {

			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {
			try {
				filereader1.close();
			} catch (IOException i) {

				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		//支店別集計ファイル作成出力処理
		for (Map.Entry<String, String> entry : map1.entrySet()) {

			if (map2.containsKey(entry.getKey())) {
				String uriagesyukei = entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey());
				uriagesyukeiList.add(uriagesyukei);

			} else {
				String uriagesyukei = entry.getKey() + "," + entry.getValue() + "," + "0";
				uriagesyukeiList.add(uriagesyukei);
			}

		}

		try {
			File file2 = new File(args[0], "branch.out");
			filereader3 = new BufferedWriter(new FileWriter(file2));

			for (int l = 0; l < uriagesyukeiList.size(); l++) {
				filereader3.write(uriagesyukeiList.get(l) + System.lineSeparator());

			}

		} catch (IOException i) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				filereader3.close();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				System.out.println("予期せぬエラーが発生しました");
				return;

			}
		}

	}
}
