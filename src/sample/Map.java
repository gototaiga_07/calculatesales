package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Map {
	public static void main(String[] args) {

		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, String> map2 = new HashMap<String, String>();
		BufferedReader filereader1 = null;
		BufferedReader filereader2 = null;
		String line;
		String line2;
		String line3;
		File file = new File("C://pleiades//workspace//HelloWorld//src//branch.lst");

		try {

			if (file.exists()) {

				String[] filedata = null;
				filereader1 = new BufferedReader(new FileReader(file));

				while ((line = filereader1.readLine()) != null) {

					filedata = line.split(",");

					if (filedata[0].matches("[0-9]{3}") && filedata.length == 2) {

						map.put(filedata[0], filedata[1]);

					} else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}

			} else {
				System.out.println(map);
				System.out.println("支店定義ファイルが存在しません");
				return;

			}

		} catch (IOException e) {

			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				filereader1.close();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				return;
			}
		}
		System.out.println(map);

		File list = new File("C://pleiades//workspace//HelloWorld//src//00000001.rcd");
		int sum = 0;
		try {
			filereader2 = new BufferedReader(new FileReader(list));

			line = filereader2.readLine();
			line2 = filereader2.readLine();
			line3 = filereader2.readLine();

			if (!(line3 == null)) {
				System.out.println("fileのフォーマットが不正です");
			}

			//支店コード処理
			if (map.containsKey(line)) {
				map2.put(line, line2);
			} else {
				System.out.println("の支店コードが不正です");
			}

			if (map2.containsKey(line)) {
				int motone = Integer.parseInt(map2.get(line));
				int atone = Integer.parseInt(line2);

				sum = motone + atone;
			}

			if (String.valueOf(sum).matches("[0-9]{10,}")) {
				System.out.println("合計金額が10桁を超えました");
			} else {

				String result = String.valueOf(sum);

				map2.put(line, result);
			}

		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			try {
				filereader1.close();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				System.out.println("予期せぬエラーが発生しました");
			}
			System.out.println(map2);
		}

	}
}
